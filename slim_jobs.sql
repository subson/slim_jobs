-- MySQL dump 10.13  Distrib 5.6.25, for osx10.9 (x86_64)
--
-- Host: localhost    Database: slim_jobs
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `cid` varchar(255) DEFAULT NULL,
  `data` longblob,
  `expire` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache`
--

LOCK TABLES `cache` WRITE;
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
INSERT INTO `cache` VALUES ('get_all_jobs','a:4:{i:0;a:7:{s:6:\"job_id\";s:1:\"1\";s:12:\"submitter_id\";s:1:\"1\";s:12:\"processor_id\";N;s:8:\"priority\";s:1:\"1\";s:4:\"data\";s:12:\"job 1 script\";s:7:\"created\";s:19:\"2018-09-23 15:35:42\";s:12:\"last_updated\";N;}i:1;a:7:{s:6:\"job_id\";s:1:\"2\";s:12:\"submitter_id\";s:1:\"2\";s:12:\"processor_id\";N;s:8:\"priority\";s:1:\"2\";s:4:\"data\";s:12:\"job 2 script\";s:7:\"created\";s:19:\"2018-09-23 15:35:52\";s:12:\"last_updated\";N;}i:2;a:7:{s:6:\"job_id\";s:1:\"3\";s:12:\"submitter_id\";s:1:\"3\";s:12:\"processor_id\";N;s:8:\"priority\";s:1:\"3\";s:4:\"data\";s:12:\"job 3 script\";s:7:\"created\";s:19:\"2018-09-23 15:36:01\";s:12:\"last_updated\";N;}i:3;a:7:{s:6:\"job_id\";s:1:\"4\";s:12:\"submitter_id\";s:1:\"4\";s:12:\"processor_id\";N;s:8:\"priority\";s:1:\"4\";s:4:\"data\";s:12:\"job 4 script\";s:7:\"created\";s:19:\"2018-09-23 15:36:08\";s:12:\"last_updated\";N;}}',60,1537790431);
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_queue`
--

DROP TABLE IF EXISTS `job_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_queue` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `submitter_id` int(11) DEFAULT NULL,
  `processor_id` int(11) DEFAULT NULL,
  `priority` int(5) DEFAULT NULL,
  `data` text,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_queue`
--

LOCK TABLES `job_queue` WRITE;
/*!40000 ALTER TABLE `job_queue` DISABLE KEYS */;
INSERT INTO `job_queue` VALUES (1,1,NULL,1,'job 1 script','2018-09-23 19:35:42',NULL),(3,3,NULL,3,'job 3 script','2018-09-23 19:36:01',NULL),(4,4,NULL,4,'job 4 script','2018-09-23 19:36:08',NULL);
/*!40000 ALTER TABLE `job_queue` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-24  8:09:56
