<?php
// DIC configuration

require_once __DIR__ . '/../app/src/Controller/JobController.php';
require_once __DIR__ . '/../app/src/Controller/JobProcessor.php';
use App\Controller\JobController;
use App\Controller\JobProcessor;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {

    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// PDO database library
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $pdo = new PDO("mysql:host=" . $settings['host'] . ";dbname=" . $settings['dbname'],
        $settings['user'], $settings['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['jobController'] = function ($c) {
	$jobController = new JobController($c->db);
	return $jobController;
};

$container['jobProcessor'] = function ($c) {
	$jobProcessor = new JobProcessor($c->db);
	return $jobProcessor;
};
