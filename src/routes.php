<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
// list all jobs
$app->get('/jobs', function ($request, $response, $args) {
	$results = $this->jobController->getAllJobs();
    return $this->response->withJson($results);
});

// get job by id
$app->get('/job/[{id}]', function ($request, $response, $args) {
    $results = $this->jobController->getJobByID($args);
    return $this->response->withJson($results);
});

// add new job
$app->post('/job', function ($request, $response) {
    $results = $this->jobController->addJob($request);
    return $this->response->withJson($results);
});

// delete a job
$app->get('/delete_job/[{id}]', function ($request, $response, $args) {
    $results = $this->jobController->deleteJob($args);
    return $this->response->withJson($results);
});

//update job
$app->put('/job/[{id}]', function ($request, $response, $args) {
    $results = $this->jobController->updateJob($request, $args);
    return $this->response->withJson($results);
});

//process job
$app->get('/process_job', function ($request, $response, $args) {
    $results = $this->jobProcessor->processJob($request, $args);
    return $this->response->withJson($results);
});


$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

