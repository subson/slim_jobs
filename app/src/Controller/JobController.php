<?php

namespace App\Controller;

class JobController {

	private $db;
	public $expire = "60"; 	// cache results for 60 seconds.

	function __construct($db) {
		$this->db = $db;
	}

	function cache_set($cache_id, $data) {
		$data = serialize($data);
		$sql = "INSERT INTO cache(cid, data, expire, created) VALUES(:cid, :data, :expire, :created)";
		$query = $this->db->prepare($sql);
	    $query->bindParam("cid", $cache_id);
	    $query->bindParam("data", $data);
	    $query->bindParam("expire", $this->expire);
	    $query->bindParam("created", time());
	    $query->execute();
	}

	function cache_get($cache_id) {
		$sql = "SELECT * FROM cache where cid = :cid";
		$query = $this->db->prepare($sql);
		$query->bindParam("cid", $cache_id);
		$query->execute();
		$results = $query->fetchObject();
		if ($results) {
		    return unserialize($results->data);
		}
		else {
			return [];
		}
	}

	function sanitize($input) {
		return htmlspecialchars($input);
	}

	function getAllJobs() {
		$cid = "get_all_jobs";
		try {
			$results = $this->cache_get($cid);

			if (count($results) > 0) {
				return $results;
			}
			else {
				$query = $this->db->prepare("SELECT job_id, submitter_id, processor_id, priority, data, created, last_updated FROM job_queue ORDER BY priority ASC");
		    	$query->execute();
		    	$rows = $query->rowCount();
		    	if ($rows) {
		    		$results = $query->fetchAll();
		    		$this->cache_set($cid, $results);
		    		return $results;
		    	}
		    	else {
		    		// For no jobs found, we are returning empty array
		    		return [];
		    	}
		    }
	    } catch (PDOException $e) {
	    	return $this->error($e->getMessage());
	    }
	}

	function getJobByID($args) {
		try {
			$query = $this->db->prepare("SELECT * FROM job_queue WHERE job_id=:id");
		    $query->bindParam("id", $args['id']);
		    $query->execute();

		    $results = $query->fetchObject();
		    if ($results) {
		    	return $results;
		    }
		    else {
		    	return "Job Id - {$args['id']} not found!";
		    }
		} catch (PDOException $e) {
			return $this->error($e->getMessage());
		}
	}

	function addJob($request) {
		$input = $request->getParsedBody();
	    $sql = "INSERT INTO job_queue (submitter_id, priority, data) VALUES (:submitter_id, :priority, :data)";
	    $query = $this->db->prepare($sql);
	    $query->bindParam("submitter_id", $this->sanitize($input['submitter_id']));
	    $query->bindParam("priority", $this->sanitize($input['priority']));
	    $query->bindParam("data", $this->sanitize($input['data']));
	    $query->execute();
	    $input['job_id'] = $this->db->lastInsertId();
	    return $input;
	}

	function deleteJob($args) {
		$query = $this->db->prepare("DELETE FROM job_queue WHERE job_id=:id");
	    $query->bindParam("id", $this->sanitize($args['id']));
	    $query->execute();
	    $rows = $query->rowCount();
	    if ($rows) {
	    	return "job id - {$args['id']} deleted successfully.";
	    }
	    else {
	    	// no job found with id.
	    	return [];
	    }
	}

	function updateJob($request, $args) {
		$input = $request->getParsedBody();
        $sql = "UPDATE job_queue SET  priority=:priority, data=:data, last_updated=:last_updated WHERE job_id=:id and submitter_id=:submitter_id";
        $query = $this->db->prepare($sql);
        $query->bindParam("id", $this->sanitize($args['id']));
        $query->bindParam("submitter_id", $this->sanitize($input['submitter_id']));
        $query->bindParam("priority", $this->sanitize($input['priority']));
        $query->bindParam("data", $this->sanitize($input['data']));
        $query->bindParam("last_updated", time());
        $query->execute();
        $input['id'] = $args['id'];

        return $input;
	}

}