<?php

namespace App\Controller;

class JobProcessor {
	private $db;

	function __construct($db) {
		$this->db = $db;
	}

	function processJob() {
		
		try {
			$query = $this->db->prepare("SELECT job_id, submitter_id, processor_id, priority, data, created, last_updated FROM job_queue WHERE processor_id = '' ORDER BY priority ASC LIMIT 1");
	    	$query->execute();
	    	$rows = $query->rowCount();
	    	if ($rows) {
	    		$results = $query->fetchAll();
	    		// update the job with processor_id 
	    		$update_query = "UPDATE job_queue SET processor_id=:processor_id where job_id = :id";
	    		$update_query = $this->db->prepare($sql);
        		$update_query->bindParam("id", $args['id']);
        		$update_query->bindParam("processor_id", $input['processor_id']);
        		$update_query->execute();
	    		return $results;
	    	}
	    	else {
	    		// For no jobs found to process.
	    		return [];
	    	}
	    } catch (PDOException $e) {
	    	return $this->error($e->getMessage());
	    }
	}
}